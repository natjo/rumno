#! /bin/sh

echo "> Remove binaries from system path (/usr/local/bin), root privileges required"
sudo rm /usr/local/bin/rumno
sudo rm /usr/local/bin/rumno-background
